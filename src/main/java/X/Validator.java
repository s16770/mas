package X;

import java.util.regex.Pattern;

public class Validator {


    public static boolean validPesel(String pesel) {
//        if (pesel.length() != 11) return false;
        int[] wagi = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        return pesel.length() != 11 ? false : true;
    }

    public String validateFieldsInNewForm() {
        return "";
    }

    /**
     * Validates String which should has only text with no numbers and special chars.
     * @param value
     * @return
     */
    public boolean validatePersonalData(String value) {
        Pattern pattern = Pattern.compile("[a-zA-z]+");
        return pattern.matcher(value).matches();
    }

    public boolean validatePhoneNumber(String number) {
        return number.length() == 9 ? true : false;
    }

    public boolean validateBankAccountNumber(String n) {
        return n.length() == 26 ? true : false;
    }

    public boolean validateIDCardNumber(String n) {
        return n.length() == 9 ? true : false;
    }
}
