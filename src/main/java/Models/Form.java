package Models;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "form")
public class Form extends RecursiveTreeObject<Form> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "docNo", nullable = false, length = 10)
    private String documentNo;

    @Column(name = "registerDate", nullable = false)
    private Timestamp registerDate;

    @Column(name = "status", nullable = false, length = 50)
    private String status;

    @Column(name = "note", length = 250)
    private String clientNote;

    @OneToMany(mappedBy = "form", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Set<AttributeForm> attributeSet = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "Person_id")
    private Person person;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "Product_id")
    private Product product;

    public Form() {
    }

    public Form(String documentNo, Timestamp registerDate, String status) {
        this.documentNo = documentNo;
        this.registerDate = registerDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getDocumentNo() {
        return documentNo;
    }

    public void setDocumentNo(String documentNo) {
        this.documentNo = documentNo;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClientNote() {
        return clientNote;
    }

    public void setClientNote(String clientNote) {
        this.clientNote = clientNote;
    }

    public void addAttributeSet(Attribute a) {
    }

    public Set<AttributeForm> getAttributeSet(){
        return attributeSet;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
