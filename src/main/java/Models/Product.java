package Models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "code", nullable = false, length = 10)
    private String code;

    @Column(name = "name", nullable = false, length = 250)
    private String name;

    @Column(name = "symbol", nullable = false, length = 25)
    private String symbol;

    @Column(name = "isActive", nullable = false, length = 1)
    private char isActive;

    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinTable(name="attribute_product",
            joinColumns={@JoinColumn(name="Product_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="Attribute_id", referencedColumnName="id")})
    private List<Attribute> attributeSet = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "Worker_id")
    private Worker worker;

    public Product() {
    }

    public Product(String code, String name, String symbol, char isActive) {
        this.code = code;
        this.name = name;
        this.symbol = symbol;
        this.isActive = isActive;
    }

    public List<Attribute> getAttributeSet() {
        return attributeSet;
    }

    public void addToAttributeSet(Attribute attribute) {
        addToAttributeSet(attribute, 0);
    }

    public void addToAttributeSet(Attribute attribute, int counter) {
        attributeSet.add(attribute);
        if(counter > 1) attribute.addToProductSet(this, ++counter);
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public char getIsActive() {
        return isActive;
    }

    public void setIsActive(char isActive) {
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }
}
