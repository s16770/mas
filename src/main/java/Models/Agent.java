package Models;

import javax.persistence.*;

@Entity
@Table(name = "agent")
public class Agent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "agentNo", length = 50, nullable = false)
    private String agentNo;

    @OneToOne
    @JoinColumn
    @Transient
    private Agency idAgency;

    public Agent() {
    }

    public Agent(String agentNo) {
        this.agentNo = agentNo;
    }
}
