package Models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "registerDate", nullable = false)
    private Timestamp registerDate;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "Address_id")
    private Address idAddress;

    public Client() {
    }

    public Client(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    public int getId() {
        return id;
    }

    public Timestamp getRegisterDate() {
        return registerDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRegisterDate(Timestamp registerDate) {
        this.registerDate = registerDate;
    }

    public Address getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(Address idAddress) {
        this.idAddress = idAddress;
    }
}
