package Models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "attribute")
public class Attribute {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "code", nullable = false, length = 25)
    private String code;

    @Column(name = "type", nullable = false, length = 50)
    private String type;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "description", nullable = false, length = 250)
    private String description;

    @Column(name = "required", nullable = false, length = 1)
    private char isRequired;

    @OneToMany(mappedBy = "attribute")
    private Set<AttributeForm> formSet = new HashSet<>();

    @OneToMany(cascade=CascadeType.ALL)
    @JoinTable(name="attribute_product",
            joinColumns={@JoinColumn(name="Attribute_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="Product_id", referencedColumnName="id")})
    private List<Product> productSet = new ArrayList<>();

    public Attribute() {
    }

    public Attribute(String code, String type, String name, String description) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.description = description;
        this.isRequired = 'F';
    }

    public Attribute(String code, String type, String name, String description, char isRequired) {
        this.code = code;
        this.type = type;
        this.name = name;
        this.description = description;
        this.isRequired = isRequired;
    }

    public void setFormSet(Set<AttributeForm> formSet) {
        this.formSet = formSet;
    }

    public Set<AttributeForm> getFormSet() {
        return formSet;
    }

    public List<Product> getProductSet() {
        return productSet;
    }

    public void addToProductSet(Product product) {
        addToProductSet(product, 0);
    }

    public void addToProductSet(Product product, int counter) {
        productSet.add(product);
       if(counter > 1) product.addToAttributeSet(this, ++counter);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public char getIsRequired() {
        return isRequired;
    }

    public boolean isRequired() {
        return isRequired == 'T';
    }

    public void setIsRequired(char isRequired) {
        this.isRequired = isRequired;
    }

    public void setProductSet(List<Product> productSet) {
        this.productSet = productSet;
    }
}
