package Models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "docNo", nullable = false, length = 7)
    private String documentNo;

    @Column(name = "registerDate", nullable = false)
    private Timestamp registerDate;

    @OneToOne
    @JoinColumn
    @Transient
    private OrderDefinition idOrderDefinition;

    @OneToOne
    @JoinColumn
    @Transient
    private Form idForm;

    @OneToOne
    @JoinColumn
    @Transient
    private Worker acceptedBy;

    @OneToOne
    @JoinColumn
    @Transient
    private Person createdBy;

}
