package Models;

import javax.persistence.*;

@Entity
@Table(name = "agency")
public class Agency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false, length = 250)
    private String name;

    @Column(name = "regon", nullable = false, length = 9)
    private String regon;

    @Column(name = "nip", nullable = false, length = 10)
    private String nip;

    @Column(name = "phone", nullable = false, length = 9)
    private String phone;

    @Column(name = "mail", nullable = false, length = 100)
    private String mail;

    @OneToOne
    @JoinColumn
    @Transient
    private Address idAddress;

    public Agency() {
    }

    public Agency(String name, String regon, String nip, String phone, String mail) {
        this.name = name;
        this.regon = regon;
        this.nip = nip;
        this.phone = phone;
        this.mail = mail;
    }
}
