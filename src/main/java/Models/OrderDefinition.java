package Models;

import javax.persistence.*;

@Entity
@Table(name = "orderdefinition")
public class OrderDefinition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "code", nullable = false)
    private String code;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "isActive", nullable = false)
    private String isActive;

    @OneToOne
    @JoinColumn
    private Worker createdBy;

    public OrderDefinition() {
    }

    public OrderDefinition(String code, String name) {
        this.code = code;
        this.name = name;
    }
}
