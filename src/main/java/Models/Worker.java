package Models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "worker")
public class Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "employedSince", nullable = false)
    private Timestamp employedSince;

    @Column(name = "employedUntil")
    private Timestamp employedUntil;

    public Worker() {
    }

    public Worker(Timestamp employedSince) {
        this.employedSince = employedSince;
    }

    public Worker(Timestamp employedSince, Timestamp employedUntil) {
        this.employedSince = employedSince;
        this.employedUntil = employedUntil;
    }

    public Integer getId() {
        return id;
    }

    public Timestamp getEmployedSince() {
        return employedSince;
    }

    public void setEmployedSince(Timestamp employedSince) {
        this.employedSince = employedSince;
    }

    public Timestamp getEmployedUntil() {
        return employedUntil;
    }

    public void setEmployedUntil(Timestamp employedUntil) {
        this.employedUntil = employedUntil;
    }
}
