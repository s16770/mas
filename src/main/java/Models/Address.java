package Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "address")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "street", nullable = false, length = 200)
    private String street;

    @Column(name = "buildNo", nullable = false, length = 8)
    private String buildNo;

    @Transient
    @Column(name = "flatNo", length = 8)
    private String flatNo;

    @Column(name = "zipCode", nullable = false, length = 6)
    private String zipCode;

    @Column(name = "city", nullable = false, length = 200)
    private String city;

    @Column(name = "country", nullable = false, length = 100)
    private String country;

    public Address() {
    }

    public Address(String street, String buildNo, String zipCode, String city, String country) {
        this.street = street;
        this.buildNo = buildNo;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }

    public Address(String street, String buildNo, String zipCode, String city, String country, String flatNo) {
        this.street = street;
        this.buildNo = buildNo;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.flatNo = flatNo;
    }

    public Integer getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildNo() {
        return buildNo;
    }

    public void setBuildNo(String buildNo) {
        this.buildNo = buildNo;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
