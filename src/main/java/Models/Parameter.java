package Models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "parameter")
public class Parameter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @Column(name = "value", nullable = false, length = 250)
    private String value;

    @Column(name = "deleted", nullable = false, length = 1)
    private char deleted;

    @Column(name = "dataFrom", nullable = false)
    private Timestamp dataFrom;

    @Column(name = "dataTo", nullable = false)
    private Timestamp dataTo;

    public Parameter() {
    }

    public Parameter(String code, String value, Timestamp dataFrom, Timestamp dataTo) {
        this.code = code;
        this.value = value;
        this.dataFrom = dataFrom;
        this.dataTo = dataTo;
        this.deleted = 'F';
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public char getDeleted() {
        return deleted;
    }

    public void setDeleted(char deleted) {
        this.deleted = deleted;
    }

    public Timestamp getDataFrom() {
        return dataFrom;
    }

    public void setDataFrom(Timestamp dataFrom) {
        this.dataFrom = dataFrom;
    }

    public Timestamp getDataTo() {
        return dataTo;
    }

    public void setDataTo(Timestamp dataTo) {
        this.dataTo = dataTo;
    }
}
