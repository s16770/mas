import Models.Address;
import Models.Person;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.HashSet;
import java.util.Set;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Views/loginPanel.fxml"));
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setTitle("Insurex 2.0");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(true);
        primaryStage.centerOnScreen();
        primaryStage.show();

    }



    public static void main(String[] args) {
//        TestUtil x = new TestUtil();
//        x.connect();
        launch(args);
    }

}
