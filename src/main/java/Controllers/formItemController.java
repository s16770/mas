package Controllers;

import Models.Form;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.text.SimpleDateFormat;
import java.util.*;

public class formItemController {

    @SuppressWarnings("unused")
    private static SessionFactory sessionFactory;

    @FXML
    private AnchorPane rootAnchorPane;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXButton addNotebutton;
    @FXML
    private JFXButton editButton;

    @FXML
    private Label docNo;

    @FXML
    private Label productLabel;

    @FXML
    private Label dataLabel;

    @FXML
    private Label statusLabel;

    public Form form;

    public void init(Form f) {
        form = f;
        dataLabel.setText(
                new SimpleDateFormat("yyyy-dd-MM HH:mm:ss").format(form.getRegisterDate())
        );
        docNo.setText(form.getDocumentNo());
        productLabel.setText(form.getProduct().getName());
        statusLabel.setText(form.getStatus());
        if(form.getStatus().equals("Waiting")) addNotebutton.setVisible(true);
        if(form.getStatus().equals("Waiting")) editButton.setVisible(true);

    }

    @FXML
    public void addNote(MouseEvent e) {
        JFXDialogLayout content = new JFXDialogLayout();
        Text title = new Text("Add note for worker. Form: " + form.getDocumentNo());
        JFXTextArea textArea = new JFXTextArea();
        if(!form.getClientNote().isEmpty()) textArea.setText(form.getClientNote());
        content.setHeading(title);
        content.setBody(textArea);
        content.setPrefWidth(800);

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton saveButton = new JFXButton("Save");
        JFXButton cancelButton = new JFXButton("Cancel");

        saveButton.setOnAction(event -> {
            saveNoteToForm(textArea.getText());
            dialog.close();
        });
        cancelButton.setOnAction(event -> dialog.close());

        textArea.setPrefHeight(50);
        List<JFXButton> buttons = new ArrayList<>(
                Arrays.asList(saveButton, cancelButton)
        );

        content.setActions(buttons);
        dialog.show();
    }

    public void saveNoteToForm(String note) {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            form.setClientNote(note);
            session.update(form);
            tx.commit();

        }
        catch (HibernateException ex) {
            if (tx!=null) tx.rollback();
            System.out.println(ex);
        }
        finally {
            session.close();
        }
    }

}
