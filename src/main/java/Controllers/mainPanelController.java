package Controllers;

import Models.*;
import com.jfoenix.controls.*;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.query.Query;

import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.*;

public class mainPanelController {
    @SuppressWarnings("unused")
    private static SessionFactory sessionFactory;
    private double x, y;
    public Person p;

    @FXML
    public AnchorPane mainPane;

    @FXML
    private VBox formItems = null;

    @FXML
    public StackPane stackPane;

    @FXML
    public Label welcomeLabel;

    public List<Form> forms  = new ArrayList<>();
    public List<Product> products = new ArrayList<>();

    public void init(Person person) {
        this.p = person;
        String welcome = "Welcome " + person.getFirstName() + " " + person.getLastName();
        welcomeLabel.setText(welcome);
        displayForms();

    }


    public void displayForms(){
        findAllFormsWithProductName();
        Node node;
        FXMLLoader loader;
        formItemController formItemController;
        for (Form f :
                forms) {
            try {
                loader = new FXMLLoader(getClass().getResource("../Views/formItem.fxml"));
                node = loader.load();
                formItemController = loader.getController();
                formItemController.init(f);
                formItems.getChildren().add(node);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void pressed(MouseEvent e) {
        x = e.getSceneX();
        y = e.getSceneY();
    }

    @FXML
    void dragged(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setX(e.getScreenX() - x );
        stage.setY(e.getScreenY() - y );
    }

    @FXML
    void close(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    void minimize(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setIconified(true);

    }

    @FXML
    void logout(MouseEvent e) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("../Views/loginPanel.fxml"));
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setScene(new Scene(root));
        stage.centerOnScreen();
    }

    @FXML
    public void createNewFormDialog(MouseEvent e) {
        findAllAvailableProducts();
        JFXDialogLayout content = new JFXDialogLayout();
        Text title = new Text("Choose product");
        ChoiceBox<String> availableProducts = new ChoiceBox<>();
        for (Product product :
                products) {
            availableProducts.getItems().add(product.getName());
        }

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton chooseButton = new JFXButton("Choose product");
        JFXButton cancelButton = new JFXButton("Cancel");

        chooseButton.setOnAction(event ->  {
                String products = availableProducts.getSelectionModel().getSelectedItem();
                if(products != null) {
                    try {
                        showNewFormPanel(products);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            });
        cancelButton.setOnAction(event -> dialog.close());

        content.setActions(new ArrayList<>(Arrays.asList(chooseButton, cancelButton)));
        content.setHeading(title);
        content.setBody(availableProducts);
        content.setStyle("-fx-background-color:  #D3DAF0");
        title.setStyle("-fx-text-base-color: #EFEADD");
        availableProducts.setStyle("-fx-font: 16 arial;");

        dialog.show();
    }

    private void showNewFormPanel(String productName) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/addNewFormPanel.fxml"));
        Stage stage = (Stage) mainPane.getScene().getWindow();
        Parent root = loader.load();
        addNewFormController addNewFormController = loader.getController();
        Product choosenProduct = null;
        for (Product p :
                products) {
            if(p.getName().equals(productName)) {
                choosenProduct = p;
                break;
            }
        }
        if(p != null) addNewFormController.init(choosenProduct, p);
        stage.setScene(new Scene(root));
        stage.centerOnScreen();
    }

//    private void chooseProduct(String selectedItem) {
////        Parent root = FXMLLoader.load(getClass().getResource("../Views/addNewFormPanel.fxml"));
////        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
////        stage.setScene(new Scene(root));
////        System.out.println(selectedItem);
//    }

    public void findAllFormsWithProductName() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        try {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Form> criteriaQuery = criteriaBuilder.createQuery(Form.class);
            Root<Form> form = criteriaQuery.from(Form.class);
            criteriaQuery.select(form).where(criteriaBuilder.equal(form.get("person"), p.getId()));
            criteriaQuery.orderBy(criteriaBuilder.desc(form.get("registerDate")));
            Query<Form> query = session.createQuery(criteriaQuery);
            forms = query.getResultList();
        }
        catch (HibernateException ex) {
            System.out.println(ex);
        }
        finally {
            session.close();
        }
    }

    public void findAllAvailableProducts() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        try {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
            Root<Product> product = criteriaQuery.from(Product.class);
            criteriaQuery.select(product);
            Query<Product> query = session.createQuery(criteriaQuery);
            products = query.getResultList();
        }
        catch (HibernateException ex) {
            System.out.println(ex);
        }
        finally {
            session.close();
        }
    }






}
