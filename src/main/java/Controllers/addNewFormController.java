package Controllers;

import Models.*;
import X.Validator;
import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.*;
import java.util.List;

public class addNewFormController {
    @SuppressWarnings("unused")
    private static SessionFactory sessionFactory;
    private double x, y;
    BufferedImage bufferedImage;


    final String PATH = System.getProperty("user.dir") + "\\images\\";

    @FXML
    private VBox vbox = null;

    @FXML
    private Pane pane;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private Label productLabel;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXButton backButton;

    private Product product;
    private Person person;
    List<AnchorPane> items = new ArrayList<>();
    Map<String, List<Object>> attributes = new LinkedHashMap <>();
    LinkedList<JFXTextField> textFieldLinkedList = new LinkedList<>();
    LinkedList<JFXButton> buttonLinkedList = new LinkedList<>();
    LinkedList<JFXCheckBox> checkBoxLinkedList = new LinkedList<>();
    Map<Attribute,Map<String, LinkedList<JFXTextField>>> name = new LinkedHashMap<>();
    Map<String, String> values = new LinkedHashMap<>();


//    public void initialize(URL location, ResourceBundle resources) {
//        createAttributeField("textfield","First name", "firstName");
//        createAttributeField("textfield","Last name", "lastName");
//        createAttributeField("textfield","Pesel", "pesel");
//        createAttributeField("textfield","Phone", "phone");
//        createAttributeField("textfield","Bank Account number", "bankAccountNo");
//        createAttributeField("textfield","Bank name", "bankName");
//        createAttributeField("textfield","ID card number", "idCardNo");
//        createAttributeField("button","Add ID card scan", "idCardScan");
//        createAttributeField("checkbox","I agree to our Terms of Use, Privacy Policy and Disclaimer", "agreement");
//    }

    @FXML
    private void pressed(MouseEvent e) {
        x = e.getSceneX();
        y = e.getSceneY();
    }

    @FXML
    private void dragged(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setX(e.getScreenX() - x );
        stage.setY(e.getScreenY() - y );
    }

    @FXML
    private void close(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    private void minimize(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setIconified(true);

    }

    @FXML
    private void backToMainPanel(MouseEvent e) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/mainPanel.fxml"));
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        Parent root = loader.load();
        mainPanelController controller = loader.getController();
        controller.init(person);
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.centerOnScreen();
    }

    @FXML
    public void createNewForm(MouseEvent e){
        Form form;

        for (JFXTextField x:
                textFieldLinkedList) {
            values.put(x.getId(), x.getText());
            System.out.println(x.getPromptText() + ": " + x.getText() + " (" + x.getId() + ")");
        }
        for (JFXCheckBox x:
                checkBoxLinkedList) {
            values.put(x.getId(), x.isSelected() ? "T" : "F");
        }

        if(validateFields()) {
            sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            try {
                form = new Form("9999123458", new Timestamp(System.currentTimeMillis()), "Waiting");
                form.setPerson(person);
                form.setProduct(product);
                session.save(form);
                for (Attribute a :
                        product.getAttributeSet()) {
                    if (values.containsKey(a.getCode())) {
                        AttributeForm attributeForm = new AttributeForm(values.get(a.getCode()));
                        attributeForm.setForm(form);
                        attributeForm.setAttribute(a);
                        session.save(attributeForm);
                    }
                }
            }
            catch (HibernateException ex) {
                System.out.println(ex);
            }
            finally {
                alertDialog("Success!", "You created new form.");
                session.close();
            }
        }

    }

    public boolean validateFields() {
        String alertInformation = "";
        Validator v = new Validator();
        String value;
        for (Attribute attribute :
                product.getAttributeSet()) {
            switch (attribute.getCode().toLowerCase()) {
                case "firstname":
                    if(values.containsKey("firstname")) {
                        value = values.get("firstname");
                        if(!v.validatePersonalData(value)) alertInformation+= value.isEmpty() ? "\"firstname\" is required.\n" : "Wrong chars in firstname field.\n";
                    }
                    break;
                case "lastname":
                    if(values.containsKey("lastname")) {
                        value = values.get("lastname");
                        if(!v.validatePersonalData(value)) alertInformation+= value.isEmpty() ? "\"lastname\" is required.\n" : "Wrong chars in lastname field.\n";
                    }
                    break;
                case "pesel":
                    if(values.containsKey("pesel")) {
                        value = values.get("pesel");
                        if(!v.validPesel(value)) alertInformation+= value.isEmpty() ? "\"pesel\" is required.\n" : "Invalid pesel\n";
                    }
                    break;
                case "phone":
                    if(values.containsKey("phone")) {
                        value = values.get("phone");
                        if(!v.validatePhoneNumber(value)) alertInformation+= value.isEmpty() ? "\"phone\" is required.\n" : "Incorrect phone number length.\n";
                    }
                    break;
                case "banknumber":
                    if(values.containsKey("banknumber")) {
                        value = values.get("banknumber");
                        if(!v.validateBankAccountNumber(value)) alertInformation+= value.isEmpty() ? "\"banknumber\" is required.\n" : "Incorrect banknumber length.\n";
                    }
                    break;
                case "bankname":
                    if(values.containsKey("bankname")) {
                        value = values.get("bankname");
                        if(value.isEmpty()) alertInformation+= "\"banknumber\" is required.\n";
                    }
                    break;
                case "idcardnumber":
                    if(values.containsKey("idcardnumber")) {
                        value = values.get("idcardnumber");
                        if(!v.validateIDCardNumber(value)) alertInformation+= value.isEmpty() ? "\"idcardnumber\" is required.\n" : "Invalid idcardnumber.\n";
                    }
                    break;
                case "terms":
                    if(values.containsKey("terms")) {
                        //TODO do zrobienia checkboxy.
                    }
                    break;

            }
        }
        if(!alertInformation.isEmpty()) alertDialog("Failed", alertInformation);
        System.out.println(alertInformation.isEmpty());
        return alertInformation.isEmpty();
    }

    public void init(Product product, Person person) {
        this.person = person;
        this.product = product;

        productLabel.setText("Product: " + product.getName());
        displayProductAttributes();


        vbox.setPrefHeight(items.size() * 55);
        pane.setLayoutY(vbox.getLayoutY() + vbox.getPrefHeight() + 10);
        vbox.getChildren().addAll(items);
    }

    public void displayProductAttributes() {
        double width = 560;

        for (Attribute a :
                product.getAttributeSet()) {
            switch (a.getType()) {
                case "textfield":
                    createTextField(a.getName(), width, a.getCode(), a.getType());
                    break;
                case "button":
                    createButton(a.getName(), width, a.getCode(), a.getType());
                    break;
                case "checkbox":
                    createCheckbox(a.getName(), width, a.getCode(), a.getType());
                    break;
            }
        }
        if(!textFieldLinkedList.isEmpty()) {
            for (JFXTextField tf :
                    textFieldLinkedList) {
                AnchorPane layout = new AnchorPane();
                layout.setPrefSize(width, 55);
                layout.getChildren().add(tf);
                items.add(layout);
            }
        }

        if(!buttonLinkedList.isEmpty()) {
            for (JFXButton b :
                    buttonLinkedList) {
                AnchorPane layout = new AnchorPane();
                layout.setPrefSize(width, 55);
                layout.getChildren().add(b);
                items.add(layout);
            }
        }

        if(!checkBoxLinkedList.isEmpty()) {
            for (JFXCheckBox cb :
                    checkBoxLinkedList) {
                AnchorPane layout = new AnchorPane();
                layout.setPrefSize(width, 55);
                layout.getChildren().add(cb);
                items.add(layout);
            }
        }

//        attributes.size();
//        Map.Entry<String, List<Object>> entry = attributes.entrySet().iterator().next();
//        for (Map.Entry<String, List<Object>> entry :
//                attributes.entrySet()) {
//            System.out.println(entry.getKey());
//            for (Object b :
//                    entry.getValue()) {
//                JFXTextField textField = (JFXTextField) b;
//            }
//            if(entry.getKey().equals("textfield")) {
//                AnchorPane layout = new AnchorPane();
//                List<Object> list = entry.getValue();
//                layout.getChildren().addAll((List<JFXTextField>) list);
//                items.add(layout);
//            }
//        }
        
//        for (int i = 0; i < attributes.size(); i++) {
//            for (int j = 0; j < attributes.; j++) {
//
//            }
//        }
    }

    private void createCheckbox(String displayText, double width, String code, String type) {
        JFXCheckBox checkBox = new JFXCheckBox();
        checkBox.setText(displayText);
        checkBox.setPadding(new Insets(5));
        checkBox.setPrefSize(width - 16, 40);
        checkBox.setId(code);
        checkBoxLinkedList.add(checkBox);
    }

    private void createButton(String displayText, double width, String code, String type) {
        JFXButton button = new JFXButton();
        button.setText(displayText);
        button.setPrefSize(width,50);
        button.setStyle("-fx-text-fill: #e1e1e1; -fx-background-color:  #34343D");
        button.setId(code);
        Stage stage = new Stage();

        button.setOnAction(e ->
                {
                    try {
                        fileChooserWindow(stage);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
        );
        buttonLinkedList.add(button);
    }

    private void createTextField(String displayText, double width, String code, String type) {
        JFXTextField textField = new JFXTextField();
        textField.setPromptText(displayText);
        textField.setPrefSize(width,50);
        textField.setId(code);
        textFieldLinkedList.add(textField);
    }

    private void fileChooserWindow(Stage stage) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpeg")
        );

        File selectedFile = fileChooser.showOpenDialog(stage);
        File fileToSave = new File(PATH + "123.png");

        if (selectedFile != null) {
            System.out.println(PATH);
            bufferedImage = ImageIO.read(selectedFile);
            ImageIO.write(bufferedImage, "png", fileToSave);
            if(!selectedFile.getName().endsWith(".png")) alertDialog("ERROR", "FILE EXTENSION IS WRONG! SHOULD BE .PNG");
            if(selectedFile.getName().endsWith(".png")) alertDialog("Image added", "Succesfull");
        }
//        try {
//            final Path vaultDir = selectedFile.toPath();
//            if (!Files.exists(vaultDir)) {
//                Files.createDirectory(vaultDir);
//            }
////            addVault(vaultDir, true);
//        } catch (IOException e) {
////            LOG.error("Unable to create vault", e);
//            System.out.println(e);
//        }

    }

    void alertDialog(String title, String information) {
        JFXDialogLayout content = new JFXDialogLayout();
        Text titleText = new Text(title);
        Label infoLabel = new Label(information);
        content.setHeading(titleText);
        content.setBody(infoLabel);

        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton button = new JFXButton("OK");
        content.setActions(button);
        button.setOnAction(e -> dialog.close());
        dialog.show();
    }


}
