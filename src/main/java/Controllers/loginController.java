package Controllers;

import Models.Person;
import com.jfoenix.controls.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.io.IOException;


import org.hibernate.Session;

public class loginController {

    @SuppressWarnings("unused")
    private static SessionFactory sessionFactory;
    private double x, y;
    Person p = null;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private JFXButton signinButton;

    @FXML
    private JFXTextField usernameField;

    @FXML
    private JFXPasswordField passwordField;

    @FXML
    private StackPane sp;

    @FXML
    void pressed(MouseEvent e) {
        x = e.getSceneX();
        y = e.getSceneY();
    }

    @FXML
    void dragged(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setX(e.getScreenX() - x );
        stage.setY(e.getScreenY() - y );
    }

    @FXML
    void close(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.close();
    }

    @FXML
    void minimize(MouseEvent e) {
        Stage stage = (Stage) ((Node)e.getSource()).getScene().getWindow();
        stage.setIconified(true);

    }

    @FXML
    void signIn(MouseEvent e) throws IOException {
        if(authentication()) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../Views/mainPanel.fxml"));
            Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
            Parent root = loader.load();
            mainPanelController controller = loader.getController();
            controller.init(p);
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.centerOnScreen();
        } else {
            invalidCredentialsDialog();
        }
    }

    public boolean authentication() {
        String username = usernameField.getText();
        String password = passwordField.getText();
        boolean result = false;
        sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        try {
//            p = session.createQuery(query, Person.class).setParameter("loginField", username).getSingleResult();
            p = session.byNaturalId(Person.class).using("login", username).load();
            if(p != null) result = p.getLogin().equals(username) && p.getPassword().equals(password);
        }
        catch (HibernateException ex) {
            System.out.println(ex);
        }
        finally {
            session.close();
        }
        return result;
    }

    public void invalidCredentialsDialog() {
        JFXDialogLayout content = new JFXDialogLayout();
        Text title = new Text("Invalid credentials");
        Label label = new Label("Invalid username or password!");
        content.setHeading(title);
        content.setBody(label);

        JFXDialog dialog = new JFXDialog(sp, content, JFXDialog.DialogTransition.CENTER);

        JFXButton cancelButton = new JFXButton("Understand");
        cancelButton.setOnAction(e ->dialog.close());

        content.setActions(cancelButton);
        dialog.show();
    }


}
